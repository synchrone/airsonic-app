FROM cloudron/base:0.10.0
WORKDIR /app/code

RUN apt-get update

#proftpd
RUN apt-get install -qyy openjdk-8-jre proftpd proftpd-mod-ldap
RUN rm -rf /var/log/proftpd && ln -s /run/proftpd /var/log/proftpd

#airsonic
RUN apt-get install -qyy ffmpeg
ARG VERSION
RUN ln -s /app/data/music /var/music
EXPOSE 8080
RUN curl -L -o airsonic.war https://github.com/airsonic/airsonic/releases/download/v${VERSION:-10.1.1}/airsonic.war

ADD . .
CMD [ "/app/code/start.sh" ]


