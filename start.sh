#!/bin/bash
set -e

CODE=/app/code/
DATA=/app/data/

# Prepare airsonic
PROPS=airsonic.properties
[ ! -e "${DATA}${PROPS}" ] && bash ${CODE}${PROPS} > ${DATA}${PROPS}

TRANSCODE=${DATA}transcode/
FFMPEG=${TRANSCODE}ffmpeg
[ ! -e "$FFMPEG" ] && mkdir $TRANSCODE && ln -s /usr/bin/ffmpeg $FFMPEG

MUSIC=${DATA}music
[ ! -e "$MUSIC" ] && mkdir $MUSIC

# Prepare SFTP
if [ ! -z "$SFTP_PORT" ]; then
    ## render config
    FTP_CFG=proftpd.conf
    [ ! -d "/run/proftpd" ] && mkdir /run/proftpd
    bash ${CODE}${FTP_CFG} > /run/${FTP_CFG}

    ## generate & chmod keys if necessary
    FTPDATA=${DATA}sftpd/
    if [[ ! -f "${FTPDATA}ssh_host_ed25519_key" ]]; then
        echo "Generating ssh host keys"
        mkdir -p ${FTPDATA}
        ssh-keygen -qt rsa -N '' -f ${FTPDATA}ssh_host_rsa_key
        ssh-keygen -qt dsa -N '' -f ${FTPDATA}ssh_host_dsa_key
        ssh-keygen -qt ecdsa -N '' -f ${FTPDATA}ssh_host_ecdsa_key
        ssh-keygen -qt ed25519 -N '' -f ${FTPDATA}ssh_host_ed25519_key
        chmod 0600 ${FTPDATA}*_key
        chmod 0644 ${FTPDATA}*.pub
    fi

    ## daemonize
    /usr/sbin/proftpd -c /run/${FTP_CFG}
fi

#MEM_LIMIT=$(($(cat /sys/fs/cgroup/memory/memory.limit_in_bytes)/2**20))

mkdir -p "${DATA}music"
USER=cloudron:cloudron
chown -R $USER $DATA

# OFF TO THE RACES!
/usr/local/bin/gosu $USER java \
    -Dserver.host=0.0.0.0 \
    -Dserver.port=8080 \
    -Dserver.contextPath=/ \
    -Dserver.use-forward-headers=true \
    -Dairsonic.home=$DATA \
    -Dairsonic.defaultMusicFolder="${DATA}music" \
    -Djava.awt.headless=true \
    -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -XX:MaxRAMFraction=1 -Xms256M \
    -jar /app/code/airsonic.war
